export const toNormalDateTime = ({ date }) => {
  return new Intl.DateTimeFormat("en-GB", {
    year: "numeric",
    month: "long",
    day: "2-digit",
    minute: "2-digit",
    hour: "2-digit",
  }).format(date);
};
