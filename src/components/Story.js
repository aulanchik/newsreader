/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { getStory } from "../services/api";
import { toNormalDateTime } from "../utils/dateTime";
import {
  StoryGlobal,
  StoryTitle,
  StoryMeta,
  StoryMetaElements,
} from "../assets/StoryStyles";

export const Story = ({ storyId }) => {
  const [story, setStory] = React.useState([]);

  React.useEffect(() => {
    getStory(storyId).then((data) => data && data.url && setStory(data));
  }, []);

  return story && story.url ? (
    <StoryGlobal data-testid="story">
      <StoryTitle>
        <a href={story.url}>{story.title}</a>
      </StoryTitle>

      <StoryMeta data-testid="story__by">
        <span className="story__by" data-testid="story-by">
          <StoryMetaElements color="#000">Author: </StoryMetaElements>
          {story.by}
        </span>
        <span data-testid="story-time">
          <StoryMetaElements color="#000">Posted: </StoryMetaElements>
          {story.time && toNormalDateTime(story.time)}
        </span>
      </StoryMeta>
    </StoryGlobal>
  ) : null;
};
