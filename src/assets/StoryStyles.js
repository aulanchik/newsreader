import styled from "styled-components";

export const StoryGlobal = styled.section`
  padding-top: 10px;
  margin-botton: 10px;
  border-top: 1px solid #cccccc;

  &:first-of-type {
    border-top: 0;
  }

  &:last-of-type {
    marging-bottom: 0;
    padding-bottom: 0;
  }
`;

export const StoryTitle = styled.h1`
  text-decoration: none;
  margin-bottom: 5px;
  line-height: 1.5;
  font-size: 16px;
  margin: 0;

  a {
    text-decoration: none;
    background-color: #f8dc3d;
    color: #2e2e2e;
  }
`;

export const StoryMeta = styled.div`
  font-size: italic;

  > span:first-child {
    margin-right: 10px;
  }

  > span:not(:first-child):before {
    margin: 0 5px;
    content: "";
  }

  .story__meta-bold {
    font-weight: 900;
  }
`;

export const StoryMetaElements = styled.span`
  color: ${(props) => props.color || "red"};
  font-weight: 900;
`;
