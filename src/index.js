import React from "react";
import ReactDOM from "react-dom";
import { App } from "./App";
import * as serviceWorker from "./serviceWorker";

const wrap = () => (
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

const injectPoint = document.getElementById("root");

ReactDOM.render(wrap(), injectPoint);

serviceWorker.register();
