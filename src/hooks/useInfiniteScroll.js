import React from "react";
import { TOTAL_STORIES, STORY_INCREMENT } from "../constants";
import { debounce } from "../utils/debounce";

export const useInfiniteScroll = () => {
  const [loading, setLoading] = React.useState(false);
  const [count, setCount] = React.useState(STORY_INCREMENT);

  const handleScroll = debounce(() => {
    setLoading(true);
  }, 500);

  React.useEffect(() => {
    if (!loading) return;

    count + STORY_INCREMENT >= TOTAL_STORIES
      ? setCount(TOTAL_STORIES)
      : setCount(count + STORY_INCREMENT);
    setLoading(false);
    console.log(`Currently viewing ${count} posts`);
  }, [count, loading]);

  React.useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, [handleScroll]);

  return { count };
};
