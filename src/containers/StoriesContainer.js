import React from "react";
import { Story } from "../components/Story";
import { getStoriesIds } from "../services/api";
import { useInfiniteScroll } from "../hooks/useInfiniteScroll";
import {
  GlobalStyles,
  StoriesContainerWrapper,
} from "../assets/StoriesContainerStyles";

export const StoriesContainer = () => {
  const { count } = useInfiniteScroll();
  const [storyIds, setStoryIds] = React.useState([]);

  React.useEffect(() => {
    getStoriesIds().then((data) => setStoryIds(data));
  }, []);

  return (
    <React.Fragment>
      <GlobalStyles />
      <StoriesContainerWrapper>
        <h1>News Reader</h1>
        {storyIds.slice(0, count).map((storyId) => (
          <Story key={storyId} storyId={storyId} />
        ))}
      </StoriesContainerWrapper>
    </React.Fragment>
  );
};
