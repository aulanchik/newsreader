import axios from "axios";
import { selectFields } from "../utils/selectFields";

export const BASE_URL = "https://hacker-news.firebaseio.com/v0/";
export const NEW_STORIES_URL = `${BASE_URL}newstories.json`;
export const STORY_URL = `${BASE_URL}item/`;

export const getStory = async (storyId) => {
  const result = await axios.get(`${STORY_URL + storyId}.json`);
  return selectFields(result.data);
};

export const getStoriesIds = async () => {
  const result = await axios.get(NEW_STORIES_URL);
  return result.data;
};
